<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiEndpointsTest extends TestCase
{
    use DatabaseMigrations;

    public function test_it_can_paginate_through_resources_list()
    {
        factory(\App\User::class, 10)->create();

        $this->get(api('/users?limit=3'))
            ->assertJson([
                'meta' => [
                    'per_page' => 3
                ]
            ])
        ;
    }

    public function test_it_can_order_resource_items_by_DESC()
    {
        $this->seed('TaxonomiesTableSeeder');

        $items = json_decode($this->get(api('/taxonomies?order=DESC'))->getContent())->data;
        $this->assertTrue(!$this->_is_asc_order($items));
    }

    public function test_it_can_order_resource_items_by_ASC()
    {
        $this->seed('TaxonomiesTableSeeder');

        $items = json_decode($this->get(api('/taxonomies?order=ASC'))->getContent())->data;
        $this->assertTrue($this->_is_asc_order($items));
    }

    protected function _is_asc_order($items)
    {
        if (sizeof($items) > 1) return $items[0]->id < $items[1]->id;
        return true;
    }

    public function test_it_can_display_all_data_without_pagination()
    {
        $this->seed('PostsTableSeeder');

        $this->get(api('/posts?limit=-1'))
            ->assertStatus(200)
            ->assertJsonMissing([
                'meta' => [
                    'current_page' => 1
                ]
            ])
        ;
    }
}
