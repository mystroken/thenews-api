<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Unit\AuthenticationEndpoints;

class UsersEndpointTest extends TestCase
{
    use DatabaseMigrations, AuthenticationEndpoints;


    public function test_it_can_fetch_users()
    {
        $this->seed( 'UsersTableSeeder' );

        $this->get(api('/users'))
             ->assertJsonStructure([
                 'data' => [
                     '*' => [
                         'id', 'name', 'email'
                     ]
                 ]
             ])
        ;
    }

    public function test_it_can_fetch_a_user_by_id()
    {
        $this->seed( 'UsersTableSeeder' );

        $this->get(api('/users/1'))
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'id' => 1
                ]
            ])
        ;
    }

    public function test_it_can_retrieve_user_by_username()
    {
        $this->seed( 'UsersTableSeeder' );

        $user = \App\User::find(1);

        $this->get(api('/users/' . $user->username))
            ->assertJson([
                'data' => [
                    'username' => $user->username
                ]
            ])
        ;
    }

    public function test_it_can_returns_404_code_when_a_user_doesnt_exist()
    {
        $this->get(api('/users/1'))
            ->assertStatus(404)
        ;
    }

    public function test_it_can_create_a_user()
    {
        $response = $this->_register_a_user();
        $response->assertJsonStructure(['token']);
    }


    public function test_it_can_update_a_user_data()
    {
        $response = $this->_register_a_user(); // With fake attributes.
        $output = json_decode($response->getContent());

        $this->_set_token($output->token)
            ->put(api('/users/1'), [
                'name' => 'Mystro Ken'
            ])
        ;

        $this->get(api('/users/1'))
             ->assertJson([
                 'data' => [
                     'name' => 'Mystro Ken'
                 ]
             ])
        ;
    }


    public function test_it_can_delete_a_user()
    {
        $this->seed( 'UsersTableSeeder' );

        $this->_init_authenticated_request()
             ->delete( api('/users/1') )
             ->assertStatus( 200 )
        ;
    }

    public function test_it_can_fetch_user_posts()
    {
        $this->seed( 'UsersTableSeeder' );
        $user = \App\User::find(1);

        $this->get(api('/users/' . $user->username . '/posts'))
             ->assertStatus(200)
             ->assertJsonStructure([
                 'data' => []
             ])
        ;
    }
}
