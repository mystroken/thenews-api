<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TaxonomiesEndpointsTest extends TestCase
{
    use DatabaseMigrations;

    public function test_it_can_fetch_taxonomies()
    {
        $this->seed( 'TaxonomiesTableSeeder' );

        $this->get(api('/taxonomies'))
             ->assertJsonStructure([
                 'data' => [
                     '*' => [
                         'id', 'name', 'slug', 'type'
                     ]
                 ]
             ])
        ;
    }

    public function test_it_can_fetch_taxonomies_by_types()
    {
        $this->seed( 'TaxonomiesTableSeeder' );

        $this->get(api('/taxonomies?type=country'))
             ->assertStatus(200)
             ->assertJson([
                 'data' => [
                     [
                         'type'  => 'country',
                     ]
                 ]
             ])
        ;
    }

    public function test_it_can_retrieve_a_taxonomy_by_ID()
    {
        $this->seed( 'TaxonomiesTableSeeder' );

        $this->get(api('/taxonomies/1'))
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'id' => 1,
                ]
            ])
        ;
    }

    public function test_it_can_retrieve_a_taxonomy_by_slug()
    {
        $this->seed( 'TaxonomiesTableSeeder' );

        $taxonomy = \App\Taxonomy::find(1);

        $this->get(api('/taxonomies/' . $taxonomy->slug))
             ->assertStatus(200)
             ->assertJson([
                 'data' => [
                     'slug' => $taxonomy->slug,
                 ]
             ])
        ;
    }

    public function test_it_can_fetch_taxonomy_posts()
    {
        $this->seed( 'TaxonomiesTableSeeder' );
        $taxonomy = \App\Taxonomy::find(1);

        $this->get(api('/taxonomies/' . $taxonomy->slug . '/posts'))
             ->assertStatus(200)
             ->assertJsonStructure([
                 'data' => []
             ])
        ;
    }

    public function test_it_can_returns_404_code_when_a_taxonomy_doesnt_exist()
    {
        $this->get(api('/taxonomies/1'))
             ->assertStatus(404)
        ;
    }
}
