<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ChannelsEndpointsTest extends TestCase
{
    use DatabaseMigrations;

    public function test_it_can_fetch_channels()
    {
        $this->seed( 'ChannelsTableSeeder' );

        $this->get(api('/channels'))
             ->assertJsonStructure([
                 'data' => [
                     '*' => [
                         'id', 'name', 'slug', 'description', 'url'
                     ]
                 ]
             ])
        ;
    }

    public function test_it_can_fetch_a_channel_by_ID()
    {
        $this->seed( 'ChannelsTableSeeder' );

        $this->get(api('/channels/1'))
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'id' => 1
                ]
            ])
        ;
    }

    public function test_it_can_retrieve_a_channel_by_slug()
    {
        $this->seed( 'ChannelsTableSeeder' );

        $channel = \App\Channel::find(1);

        $this->get(api('/channels/' . $channel->slug))
             ->assertStatus(200)
             ->assertJson([
                 'data' => [
                     'slug' => $channel->slug
                 ]
             ])
        ;
    }

    public function test_it_returns_404_code_when_the_channel_doesnt_exist()
    {
        $this->get(api('/channels/1'))
            ->assertStatus(404)
        ;
    }
}
