<?php


namespace Tests\Feature;


use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Tests\Unit\AuthenticationEndpoints;

class ImagesEndpointsTest extends TestCase
{
    use DatabaseMigrations, AuthenticationEndpoints;
}
