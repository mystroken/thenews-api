<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Unit\AuthenticationEndpoints;

class PostsEndpointsTest extends TestCase
{
    use DatabaseMigrations, AuthenticationEndpoints;


    protected $fakePostAttributes = [
        'title'       => 'Post test',
        'slug'        => 'post-test',
        'type'        => 'post',
        'content'     => 'The content of the post test',
    ];

    /**
     * Creates a fake post.
     *
     * CONDITIONS:
     * (1) A user must be authenticated
     * before creating a new post.
     *
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function _create_a_post()
    {
        return $this
                ->_init_authenticated_request()
                ->post( api('/posts'), $this->fakePostAttributes )
            ;
    }

    public function test_it_can_fetch_posts()
    {
        $this->seed( 'PostsTableSeeder' );

        $this->get(api('/posts'))
             ->assertStatus(200)
             ->assertJsonStructure([
                 'data' => [
                     '*' => [
                         'id',
                         'title',
                         'slug',
                         'type',
                         'description',
                         'content',
                         'image',
                         'link',
                         'published',
                     ]
                 ]
             ])
        ;
    }

    public function test_it_can_fetch_posts_by_types()
    {
        $this->seed( 'ChannelsTableSeeder' );
        $this->seed( 'PostsTableSeeder' );

        $this->get(api('/posts?type=post'))
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    [
                        'type'  => 'post',
                    ]
                ]
            ])
        ;
    }

    public function test_it_can_fetch_a_post_by_ID()
    {
        $this->seed( 'PostsTableSeeder' );

        $this->get(api('/posts/1'))
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'id' => 1
                ]
            ])
        ;
    }

    public function test_it_can_fetch_a_post_by_slug()
    {
        $this->seed( 'PostsTableSeeder' );

        $post = \App\Post::find(1);

        $this->get(api('/posts/' . $post->slug))
             ->assertStatus(200)
             ->assertJson([
                 'data' => [
                     'slug' => $post->slug
                 ]
             ])
        ;
    }

    public function test_it_returns_404_code_when_the_post_does_not_exist()
    {
        $this->get(api('/posts/1'))
             ->assertStatus(404)
        ;
    }

    /**
     * Checks if a user can create a new post.
     *
     * CONDITIONS:
     * (1) A user must be authenticated
     * before creating a new post.
     */
    public function test_it_can_create_a_post()
    {
        $response = $this->_create_a_post();
        return $response
            ->assertStatus(201)
            ->assertJson([
                'data' => [
                    'slug' => $this->fakePostAttributes['slug']
                ]
            ])
        ;
    }

    public function test_it_can_update_a_post()
    {
        // Create the fake post first.
        $this->_create_a_post();
        $updatedSlug = $this->fakePostAttributes['slug'] . '-updated';

        // Then try to edit its slug.
        $this
            ->put(api('/posts/1'), [
                'slug' => $updatedSlug
            ])
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'slug' => $updatedSlug
                ]
            ])
        ;
    }

    public function test_it_can_delete_a_post()
    {
        // Create a post.
        $this->_create_a_post();

        // Then delete it.
        $this
            ->delete(api('/posts/1'))
            ->assertStatus(200)
        ;
    }

    public function test_unauthenticated_users_cannot_create_posts()
    {
        $this
            ->post( api('/posts'), $this->fakePostAttributes )
            ->assertStatus(401)
        ;
    }
}
