<?php


namespace App\Repositories;


use App\Http\Traits\ImageFromURL;
use App\Image;
use Exception;
use Illuminate\Support\Str;
use App\Repositories\Support\Repository;
use Storage;
use Illuminate\Http\UploadedFile;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;
use Intervention\Image\Facades\Image as InterventionImage;

class ImageRepository
{
    use Repository, ImageFromURL;

    /**
     * The model of this repository.
     *
     * @var Image
     */
    protected $model;

    /**
     * The folder where to keep images.
     *
     * @var string
     */
    public static $uploadFolder = 'images';

    /**
     * Store a new upload.
     *
     * @param UploadedFile $file
     * @return Image
     */
    public function store(UploadedFile $file): Image
    {
        // Instantiate a new
        // empty model. Then fill it in
        // with the uploaded file.
        $this->model = new Image;
        return $this->save($file);
    }

    /**
     * Update an image.
     *
     * @param int $id
     * @param UploadedFile $file
     * @return Image
     */
    public function update(int $id, UploadedFile $file): Image
    {
        // Retrieve the model
        // to update. Clean it up, then
        // fill it in with the new information.
        $this->model = $this->getById($id);
        $this->clean($this->model);
        return $this->save($file);
    }

    /**
     * @param int $id
     * @return Image
     * @throws Exception
     */
    public function delete(int $id): Image
    {
        // Retrieve the model
        // to delete. Clean it up, then
        // delete the model.
        $this->model = $this->getById($id);
        $this->clean($this->model);
        $this->model->delete();
        return $this->model;
    }

    /**
     * Clean up the image.
     * Delete the physically files.
     *
     * @param Image $image The image model to clean up.
     */
    public function clean(Image $image): void
    {

    }

    /**
     * Stores the file and generates the model attributes.
     *
     * @param UploadedFile $file
     * @param string $title The image title.
     * @return array
     */
    public function storeFile(UploadedFile $file, string $title = null): array
    {
        // Optimize the image before saving it.
        ImageOptimizer::optimize($file->getPathname());

        // Store the image.
        $extension = Str::lower($file->getClientOriginalExtension());
        $name = Str::slug(basename($file->getClientOriginalName(), '.' . $extension));
        $uniqSuffix = '-' . uniqid('', false);
        $filename = $name . $uniqSuffix . '.' . $extension;
        $path = $file->storeAs(self::$uploadFolder, $filename, 'public');

        // Store thumbs.
        $image = InterventionImage::make($file->getPathname());
        $thumb_large = $this->storeThumbs($image, 1920, $name, $uniqSuffix, $extension);
        $thumb_medium = $this->storeThumbs($image, 680, $name, $uniqSuffix, $extension);
        $thumb_small = $this->storeThumbs($image, 210, $name, $uniqSuffix, $extension);

        // Return the model attributes for saving.
        return [
            'title' => $title ?? $name,
            'original' => $path,
            'large' => $thumb_large ?? $path,
            'medium' => $thumb_medium ?? $path,
            'small' => $thumb_small ?? $path,
        ];
    }

    /**
     * Store a new uploaded file.
     *
     * @param UploadedFile $file
     * @return Image
     */
    protected function save(UploadedFile $file): Image
    {
        // Store the file.
        $attributes = $this->storeFile($file);

        // Fill in the model.
        $this->model->fill($attributes);
        $this->model->save();
        return $this->model;
    }

    /**
     * Save a thumb of an image and return the path.
     *
     * @param \Intervention\Image\Image $image
     * @param int $width
     * @param string $name Filename.
     * @param string $suffix File suffix.
     * @param string $extension File extension.
     * @return null|string
     */
    protected function storeThumbs(\Intervention\Image\Image $image, int $width, string $name, string $suffix, string $extension): string
    {
        $path = self::$uploadFolder . "/{$name}-{$width}{$suffix}.{$extension}";
        $root = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
        $image->widen($width)->save($root . '/'. $path);

        return $path;
    }

    /**
     * Download an image from a given
     * URL and store it in our database.
     *
     * @param string $url
     * @return Image
     */
    public function storeFromURL(string $url): Image
    {
        return $this->store(self::getImageFromURL($url));
    }
}
