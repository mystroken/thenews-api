<?php
/**
 * Created by PhpStorm.
 * User: ken
 * Date: 2019-02-23
 * Time: 11:34
 */

namespace App\Repositories;


use Illuminate\Database\Eloquent\Relations\Relation;

class BaseRepository {
    protected $model;

    /**
     * @param Relation $relation
     * @return $this
     */
    public function from(Relation $relation)
    {
        $this->model = $relation;
        return $this;
    }
}
