<?php

namespace App\Repositories;

use App\Repositories\Support\FilterByType;
use App\Repositories\Support\Repository;
use App\Taxonomy;

class TaxonomyRepository
{
    use Repository, FilterByType;

    /**
     * @var Taxonomy
     */
    protected $model;

    /**
     * TaxonomyRepository constructor.
     */
    public function __construct()
    {
        $this->model = new Taxonomy;
    }

    /**
     * @param $input
     * @return Taxonomy
     */
    public function store($input): Taxonomy
    {
        return $this->save($this->model, $input);
    }

    /**
     * @param Taxonomy $model
     * @param array $input
     * @return Taxonomy
     */
    protected function save($model, $input): Taxonomy
    {
        $model->fill($input);
        $model->save();
        return $model;
    }
}
