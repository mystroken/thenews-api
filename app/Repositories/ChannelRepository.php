<?php

namespace App\Repositories;

use App\Aggregator\Exceptions\TransformerException;
use App\Channel;
use App\Aggregator\Support\Transformer;
use App\Post;
use App\Repositories\Support\Repository;
use React\Promise\Promise;
use willvincent\Feeds\Facades\FeedsFacade as Feeds;
use function React\Promise\all;

class ChannelRepository
{
    use Repository;

    /**
     * @var Channel
     */
    protected $model;

    /**
     * ChannelRepository constructor.
     */
    public function __construct()
    {
        $this->model = new Channel;
    }

    /**
     * @param $input
     * @return Channel
     */
    public function store($input): Channel
    {
        return $this->save($this->model, $input);
    }

    /**
     * @param Channel $model
     * @param array $input
     * @return Channel
     */
    protected function save($model, $input): Channel
    {
        $model->fill($input);
        $model->save();
        return $model;
    }

    /**
     * @param Channel $channel
     *
     * @return Promise Whose value is an array of created posts.
     * @throws TransformerException
     */
    public static function pull(Channel $channel): Promise
    {

        $transformer = Transformer::createFromChannel($channel);
        $itemsCollection = collect(Feeds::make($channel->feed)->get_items());

        // Create a collection of promises whose each value is a transformed post.
        $promises = $itemsCollection->map(static function ($item) use ($transformer) {
            return new Promise(static function ($resolve, $reject) use ($transformer, $item) {
                // Loop items, transform them,
                // save them to the database and
                // return the generated post.
                $transformer->extract($item)->then(static function (array $values) use ($resolve) {
                    /** @var Post $post */
                    $post = $values['post'];
                    $taxonomies = $values['taxonomies'];

                    $post->save();
                    if ( ! empty($taxonomies) ) {
                        $post->taxonomies()->attach($taxonomies);
                    }

                    $resolve($post);
                });
            });
        });

        // Returns a promise whose value is an array of generated posts.
        return all($promises->all());
    }
}
