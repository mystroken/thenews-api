<?php

namespace App\Repositories;

use App\User;
use Exception;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Support\Repository;
use League\Fractal\Resource\Collection;

class UserRepository
{
    use Repository;

    /**
     * @var User
     */
    protected $model;

    /**
     * UserRepository constructor.
     */
    public function __construct()
    {
        $this->model = new User;
    }

    /**
     * @param $input
     * @return User
     */
    public function store($input): User
    {
        return $this->save($this->model, $input);
    }

    /**
     * @param User $model
     * @param array $input
     * @return User
     */
    protected function save($model, $input): User
    {
        $model->fill($input);
        $model->save();
        return $model;
    }

    /**
     * Get record by the slug.
     *
     * @param  string $username
     * @return Collection
     */
    public function getByUsername($username): Collection
    {
        return $this->model->where('username', $username)->first();
    }

    /**
     * Retrieve a model by
     * its ID or its Slug.
     *
     * @param string $resourceId
     * @return Model|Collection|null
     */
    public function getByIdOrUsername($resourceId)
    {
        $ID = (int) $resourceId;

        try {
            if ($ID > 0) {
                return $this->getById($resourceId);
            }
            return $this->getByUsername($resourceId);
        } catch(Exception $e) {}

        return null;
    }
}
