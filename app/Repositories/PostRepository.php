<?php

namespace App\Repositories;

use App\Post;
use App\Repositories\Support\FilterByType;
use App\Repositories\Support\Repository;
use Exception;

class PostRepository
{
    use Repository, FilterByType;

    /**
     * @var Post
     */
    protected $model;

    /**
     * Image repository
     *
     * @var ImageRepository
     */
    protected $imageRepository;

    /**
     * PostRepository constructor.
     */
    public function __construct()
    {
        $this->model = new Post;
        $this->imageRepository = new ImageRepository;
    }

    /**
     * @param $input
     * @return Post
     */
    public function store($input): Post
    {
        // If an image has been sent,
        // Use the image repository to save it.
        // Then attach it to the model.
        dd($input);
        return $this->save($this->model, $input);
    }

    /**
     * @param int|string $id
     * @param $input
     * @return Post
     */
    public function update($id, $input): Post
    {
        // If the post is attached to an image,
        // And the current request contain a new image,
        // Then update it, else create and attach it.
        $this->model = $this->getByIdOrSlug($id);
        return $this->save($this->model, $input);
    }

    /**
     * @param int|string $id
     * @return Post
     * @throws Exception
     */
    public function delete($id): Post
    {
        // If the post has an image,
        // Delete the image too.
        $this->model = $this->getByIdOrSlug($id);
        $this->model->delete();

        return $this->model;
    }

    /**
     * @param Post $model
     * @param array $input
     * @return Post
     */
    protected function save($model, $input): Post
    {
        $model->fill($input);
        $model->save();
        return $model;
    }
}
