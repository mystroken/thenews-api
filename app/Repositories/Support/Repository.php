<?php

namespace App\Repositories\Support;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use League\Fractal\Resource\Collection;

trait Repository
{

    /**
     * @return array
     */
    public function getNumber(): array
    {
        return $this->model->count();
    }

    /**
     * @param int $id
     * @return Model
     */
    public function getById($id): Model
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Get all the records
     *
     * @return array Model
     */
    public function all(): array
    {
        return $this->model->get();
    }

    /**
     * Get number of the records
     *
     * @param  int $number
     * @param  string $sort
     * @param  string $sortColumn
     * @return Collection|LengthAwarePaginator
     */
    public function page($number = 10, $sort = 'desc', $sortColumn = 'id')
    {
        if ($number === -1) {
            return $this->model->orderBy($sortColumn, $sort)->get();
        }

        return $this->model->orderBy($sortColumn, $sort)->paginate($number);
    }

    /**
     * Get record by the slug.
     *
     * @param  string $slug
     * @return Model
     */
    public function getBySlug($slug): Model
    {
        return $this->model->where('slug', $slug)->first();
    }

    /**
     * Retrieve a model by
     * its ID or its Slug.
     *
     * @param string $resourceId
     * @return Model|Collection|null
     */
    public function getByIdOrSlug($resourceId)
    {
        $ID = (int) $resourceId;

        try {
            if ($ID > 0) {
                return $this->getById($resourceId);
            }
            return $this->getBySlug($resourceId);
        } catch(Exception $e) {}

        return null;
    }

    /**
     * @param $input
     * @return Model
     */
    public function store($input): Model
    {
        return $this->save($this->model, $input);
    }

    /**
     * @param $id
     * @param $input
     * @return Model
     */
    public function update($id, $input): Model
    {
        $this->model = $this->getById($id);
        return $this->save($this->model, $input);
    }

    /**
     * @param int $id
     * @return Model
     * @throws Exception
     */
    public function delete($id): Model
    {
        $this->model = $this->getById($id);
        $this->model->delete();

        return $this->model;
    }

    /**
     * @param Model $model
     * @param array $input
     * @return Model
     */
    protected function save($model, $input): Model
    {
        $model->fill($input);
        $model->save();
        return $model;
    }

    /**
     * Change the model to use as source.
     *
     * @param Relation $relation
     * @return $this
     */
    public function from(Relation $relation): self
    {
        $this->model = $relation;
        return $this;
    }
}
