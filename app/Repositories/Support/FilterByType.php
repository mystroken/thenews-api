<?php

namespace App\Repositories\Support;


/**
 * Trait FilterByType
 * @package App\Repositories
 * @author Mystro Ken <mystorken@gmail.com>
 */
trait FilterByType
{
    public function filterByType($type = null)
    {
        if ($type !== null) $this->model = $this->model->where('type', $type);
        return $this;
    }
}
