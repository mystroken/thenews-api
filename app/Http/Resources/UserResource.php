<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'username'    => $this->username,
            'email'       => $this->email,
            'role'        => $this->role,
            'avatar'      => $this->avatar,
            'description' => $this->description,
            'website'     => $this->website,
            'location'    => $this->location,
            'created_at'  => $this->created_at->toAtomString(),
            'updated_at'  => $this->updated_at->toAtomString(),
        ];
    }
}
