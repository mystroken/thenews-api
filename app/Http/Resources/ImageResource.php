<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Storage;

class ImageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'original' => Storage::disk('public')->url($this->original),
            'large' => Storage::disk('public')->url($this->large),
            'medium' => Storage::disk('public')->url($this->medium),
            'small' => Storage::disk('public')->url($this->small),
        ];
    }
}
