<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'channel'       => $this->when(($this->channel_id > 0), new ChannelResource($this->channel)),
            'image'         => $this->when(($this->image_id > 0), new ImageResource($this->image)),
            'title'         => $this->title,
            'slug'          => $this->slug,
            'type'          => $this->type,
            'description'   => $this->description,
            'content'       => $this->content,
            'link'          => $this->link,
            'published'     => $this->published,
            'deleted'       => $this->deleted,
            'taxonomies'    => TaxonomyResource::collection($this->taxonomies),
            'author'        => $this->when(($this->user_id > 0), new UserResource($this->author)),
            'created_at'    => $this->created_at->toAtomString(),
            'updated_at'    => $this->updated_at->toAtomString(),
        ];
    }
}
