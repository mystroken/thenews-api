<?php


namespace App\Http\Traits;


use Exception;
use Illuminate\Http\UploadedFile;

trait ImageFromURL
{
    /**
     * Get an image url, download it and return
     * it as UploadedFile type.
     *
     * @param string $url
     * @return null|UploadedFile
     */
    public static function getImageFromURL(string $url): UploadedFile
    {
        try {
            $info = pathinfo($url);
            $contents = file_get_contents($url);
            $file = '/tmp/' . $info['basename'];
            file_put_contents($file, $contents);
            return new UploadedFile($file, $info['basename']);
        } catch(Exception $exception) {}

        return null;
    }
}
