<?php


namespace App\Http\Traits;


use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

trait ImageUpload
{
    /**
     * @param UploadedFile $uploadedFile The upload file.
     * @param null $folder Folder variables.
     * @param string $disk The disk where to store.
     * @param null $filename The filename.
     * @return false|string
     */
    public function uploadOne(UploadedFile $uploadedFile, $folder = null, $filename = null, $disk = 'public')
    {
        $name = !is_null($filename) ? $filename : Str::random(25);
        $file = $uploadedFile->storeAs($folder, $name . '.' . $uploadedFile->getClientOriginalExtension(), $disk);
        return $file;
    }
}
