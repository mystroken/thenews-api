<?php

namespace App\Http\Requests\Posts;

use Illuminate\Foundation\Http\FormRequest;
use Dingo\Api\Auth\Auth;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'bail|required',
            'slug' => 'bail|required|unique:posts',
            'type' => 'bail|required',
        ];
    }

    /**
     * @return array
     */
    public function validationData(): array
    {
        $input = $this->all();

        // Add the current authenticated user as author.
        $user = app(Auth::class)->user();
        if ($user) {
            $input['user_id'] = $user->id;
        }

        // Users are supposed to create posts only
        // not news.
        $input['type'] = 'post';

        // The title must be slug-ify.
        if ($input['title']) {
            $input['slug'] = str_slug($input['title']);
        }

        $this->replace( $input );
        return parent::validationData();
    }
}
