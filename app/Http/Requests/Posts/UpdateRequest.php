<?php

namespace App\Http\Requests\Posts;

use App\Repositories\PostRepository;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        if ($this->get('slug')) $rules['slug'] = 'bail|required|unique:posts';

        return $rules;
    }

    public function validationData() {
        $input = $this->all();

        // Users are supposed to create posts only
        // not news.
        $input['type'] = 'post';

        // The title must be slugiffy.
        if ($input['title']) $input['slug'] = str_slug($input['title']);

        $this->replace( $input );
        return parent::validationData();
    }
}
