<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Resources\ChannelResource;
use App\Repositories\ChannelRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ChannelController extends ApiController
{
    /**
     * @var ChannelRepository $repository
     */
    protected $repository;

    /**
     * ChannelController constructor.
     *
     * @param ChannelRepository $channelRepository
     */
    public function __construct( ChannelRepository $channelRepository )
    {
        $this->repository = $channelRepository;
    }

    /**
     * @param Request $request
     * @return JsonResource
     */
    public function index(Request $request)
    {
        $order = $request->query( 'order' )
            ? $request->query( 'order' )
            : 'desc'
        ;

        $orderBy = $request->query( 'orderBy' )
            ? $request->query( 'orderBy' )
            : 'id'
        ;
        return ChannelResource::collection( $this->repository->page( (int) $request->query( 'limit' ), $order, $orderBy ) );
    }

    /**
     * Retrieve a single resource by
     * its ID or its Slug.
     *
     * @param string $resourceID
     * @return JsonResource
     */
    public function show($resourceID)
    {
        if ( $resource = $this->repository->getByIdOrSlug($resourceID) ) return new ChannelResource($resource);
        return $this->notFoundResourceResponse();
    }
}
