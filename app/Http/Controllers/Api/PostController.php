<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Posts\StoreRequest;
use App\Http\Requests\Posts\UpdateRequest;
use App\Http\Resources\PostResource;
use App\Repositories\PostRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PostController extends ApiController
{
    /**
     * @var PostRepository $repository
     */
    protected $repository;

    public function __construct( PostRepository $postRepository )
    {
        $this->repository = $postRepository;
    }

    /**
     * @param Request $request
     * @return JsonResource
     */
    public function index(Request $request): JsonResource
    {
        return self::listOfPostsResponse(
            $this->repository,
            $request
        );
    }

    /**
     * Retrieve a single resource by
     * its ID or its Slug.
     *
     * @param string $resourceID
     * @return JsonResource
     */
    public function show($resourceID): JsonResource
    {
        if ( $resource = $this->repository->getByIdOrSlug($resourceID) ) {
            return new PostResource($resource);
        }
        return $this->notFoundResourceResponse();
    }

    /**
     * Stores a new post to the database.
     *
     * @param StoreRequest $request
     * @return JsonResource;
     */
    public function store(StoreRequest $request): JsonResource
    {
        return new PostResource( $this->repository->store( $request->all() ) );
    }

    /**
     * Updates a post.
     *
     * @param int|string $id
     * @param UpdateRequest $request
     *
     * @return JsonResource;
     */
    public function update($id, UpdateRequest $request): JsonResource
    {
        if ( $post = $this->repository->getByIdOrSlug($id) ) {
            $user = $this->auth->user();

            if ($post->author === $user) {
                return new PostResource( $this->repository->update( $id, $request->all() ) );
            }

            return response()->json( ['error' => 'not_authorized'], 401 );
        }

        return $this->notFoundResourceResponse();
    }

    /**
     * Deletes a post.
     *
     * @param $id
     *
     * @return JsonResource
     * @throws Exception
     */
    public function destroy($id): JsonResource
    {
        return new PostResource( $this->repository->delete( $id ) );
    }

    /**
     * Return a list of posts as response.
     *
     * @param PostRepository $repository
     * @param Request $request
     *
     * @return JsonResource
     */
    public static function listOfPostsResponse(PostRepository $repository, Request $request): JsonResource
    {
        $order = $request->query( 'order' )
            ? $request->query( 'order' )
            : 'desc'
        ;

        $orderBy = $request->query( 'orderBy' )
            ? $request->query( 'orderBy' )
            : 'updated_at'
        ;

        return PostResource::collection(
            $repository
                ->filterByType($request->query( 'type' ))
                ->page( (int) $request->query( 'limit' ), $order, $orderBy )
        );
    }
}
