<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Users\StoreRequest;
use App\Http\Resources\UserResource;
use App\Repositories\PostRepository;
use App\User;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use Illuminate\Http\Resources\Json\JsonResource;
use Symfony\Component\CssSelector\Exception\InternalErrorException;

class UserController extends ApiController
{
    /**
     * @var UserRepository
     */
    protected $repository;


    /**
     * UserController constructor.
     *
     * @param UserRepository $repository
     */
    public function __construct( UserRepository $repository )
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return JsonResource
     */
    public function index(Request $request)
    {
        $order = $request->query( 'order' )
            ? $request->query( 'order' )
            : 'desc'
        ;

        $orderBy = $request->query( 'orderBy' )
            ? $request->query( 'orderBy' )
            : 'id'
        ;
        return UserResource::collection( $this->repository->page( (int) $request->query( 'limit' ), $order, $orderBy ) );
    }

    /**
     * Retrieve a single user by
     * ID or by its username.
     *
     * @param string $resourceID
     * @return JsonResource
     */
    public function show($resourceID)
    {
        if ( $resource = $this->repository->getByIdOrUsername($resourceID) ) return new UserResource($resource);
        return $this->notFoundResourceResponse();
    }


    /**
     * Store a user to the database.
     *
     * @param \App\Http\Requests\Users\StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws InternalErrorException
     */
    public function store(StoreRequest $request)
    {
        try {
            if ( $user = $this->repository->store( $request->all() ) ) {
                $token = \JWTAuth::fromUser( $user );

                return response()->json( compact( 'token' ) );
            }
        } catch (\Exception $e) {
            return response()->json( $e );
        }
        throw new InternalErrorException('internal_error');
    }


    /**
     * Updates a user data.
     *
     * @param int $id
     * @param Request $request
     * @return JsonResource
     */
    public function update($id, Request $request)
    {
        return new UserResource( $this->repository->update( (int) $id, $request->all() ) );
    }

    /**
     * Delete a user.
     *
     * @param int $id
     * @param Request $request
     *
     * @return JsonResource
     * @throws \Exception
     */
    public function destroy($id, Request $request)
    {
        return new UserResource( $this->repository->delete( (int) $id ) );
    }

    /**
     * Retrieve the posts of the
     * selected user.
     *
     * @param $resourceID
     * @param Request $request
     *
     * @return JsonResource
     */
    public function posts($resourceID, Request $request)
    {
        /**
         * @var User $user
         */
        if ($user = $this->repository->getByIdOrUsername($resourceID)) {
            return PostController::listOfPostsResponse(
                (new PostRepository)->from($user->posts()),
                $request
            );
        }

        return $this->notFoundResourceResponse();
    }

}
