<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Resources\PostResource;
use App\Http\Resources\TaxonomyResource;
use App\Repositories\PostRepository;
use App\Repositories\TaxonomyRepository;
use App\Taxonomy;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TaxonomyController extends ApiController
{
    /**
     * @var TaxonomyRepository $repository
     */
    protected $repository;

    /**
     * TaxonomyController constructor.
     *
     * @param TaxonomyRepository $taxonomyRepository
     */
    public function __construct( TaxonomyRepository $taxonomyRepository )
    {
        $this->repository = $taxonomyRepository;
    }

    /**
     * @param Request $request
     * @return JsonResource
     */
    public function index(Request $request)
    {
        $order = $request->query( 'order' )
            ? $request->query( 'order' )
            : 'desc'
        ;

        $orderBy = $request->query( 'orderBy' )
            ? $request->query( 'orderBy' )
            : 'id'
        ;

        return TaxonomyResource::collection( $this->repository->filterByType($request->query( 'type' ))->page( (int) $request->query( 'limit' ), $order, $orderBy) );
    }

    /**
     * Retrieve a single resource by
     * its ID or its Slug.
     *
     * @param string $resourceID
     * @return JsonResource
     */
    public function show($resourceID)
    {
        if ( $resource = $this->repository->getByIdOrSlug($resourceID) ) return new TaxonomyResource($resource);
        return $this->notFoundResourceResponse();
    }

    /**
     * Retrieve the posts of the
     * selected taxonomy.
     *
     * @param string $resourceID
     * @param Request $request
     *
     * @return JsonResource
     */
    public function posts($resourceID, Request $request)
    {
        /**
         * @var Taxonomy $taxonomy
         */
        if ( $taxonomy = $this->repository->getByIdOrSlug($resourceID) ) {
            return PostController::listOfPostsResponse(
                (new PostRepository)->from($taxonomy->posts()),
                $request
            );
        }

        return $this->notFoundResourceResponse();
    }
}
