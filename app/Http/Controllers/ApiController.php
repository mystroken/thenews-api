<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Dingo\Api\Routing\Helpers as ApiHelpers;

class ApiController extends BaseController
{
    use
        AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests,
        ApiHelpers
    ;

    /**
     * @param string $message
     * @return JsonResource
     */
    protected function notFoundResourceResponse($message = 'resource_not_found')
    {
        return response()->json( ['error' => $message], 404 );
    }
}
