<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Post extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'channel_id',
        'user_id',
        'image_id',
        'title',
        'slug',
        'type',
        'description',
        'content',
        'link',
        'published',
        'deleted',
        'created_at',
        'updated_at',
    ];

    /**
     * Get the related channel.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function image(): BelongsTo
    {
        return $this->belongsTo(Image::class);
    }


    /**
     * Get the related channel.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    /**
     * Get the list of related authors.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Get the list of related taxonomies.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function taxonomies()
    {
        return $this->belongsToMany(Taxonomy::class, 'post_taxonomies', 'post_id', 'taxonomy_id');
    }

    /**
     * Is the post published.
     *
     * @return bool
     */
    public function isPublished()
    {
        return $this->published === 1;
    }

    /**
     * Is the post deleted.
     *
     * @return bool
     */
    public function isDeleted()
    {
        return $this->delete === 1;
    }
}
