<?php

namespace App\Nova;

use Emilianotisato\NovaTinyMCE\NovaTinyMCE;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;

class Post extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Post';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'title',
    ];

    /**
     * The relationships that should be eager loaded on index queries.
     *
     * @var array
     */
    public static $with = [
        'author',
        'channel',
        'image',
        'taxonomies',
    ];

    public static function label() {
        return __('Articles');
    }

    public static function singularLabel() {
        return __('Article');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     * @return array
     */
    public function fields(Request $request): array
    {
        return [
            Text::make('Titre', 'title')->sortable(),
            Select::make('Type')
                ->options([
                    'post' => 'Article',
                    'news' => 'News',
                ])
                ->hideWhenUpdating()
                ->sortable(),
            BelongsTo::make('Image')->hideFromIndex(),
            BelongsTo::make('Channel'),
            BelongsTo::make('User', 'author')->sortable(),
            BelongsToMany::make('Taxonomies', 'taxonomies', Taxonomy::class)->sortable(),
            Boolean::make('Publié', 'published')->sortable(),
            NovaTinyMCE::make('Description')->hideFromIndex(),
            NovaTinyMCE::make('Contenu', 'content')->hideFromIndex(),
            DateTime::make('Publié le', 'created_at')->hideFromIndex(),
            DateTime::make('Modifié le', 'updated_at')->sortable(),
        ];
    }
}
