<?php

namespace App\Nova;

use App\Repositories\ImageRepository;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;

class Image extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Image::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'original';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'original'
    ];

    public static function label()
    {
        return __('Images');
    }

    public static function singularLabel()
    {
        return __('Image');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     * @return array
     */
    public function fields(Request $request): array
    {
        return [
            ID::make()->sortable(),
            \Laravel\Nova\Fields\Image::make('Original')
                ->disk('public')
                ->store(static function (Request $request) {
                    $imageRepository = new ImageRepository;
                    return $imageRepository->storeFile($request->original);
                }),
        ];
    }

}
