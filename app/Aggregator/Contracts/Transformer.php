<?php
namespace App\Aggregator\Contracts;

use React\Promise\Promise;

interface Transformer
{

    /**
     * Extract the necessary attributes from
     * a SimplePie item object.
     *
     * [
     *     'title',
     *     'slug',
     *     'type',
     *     'description',
     *     'content',
     *     'link',
     *     'image',
     *     'created_at',
     * ]
     *
     * @param \SimplePie_Item $item
     * @return Promise
     */
    public function extract(\SimplePie_Item $item): Promise;

    /**
     * Implement this method to add a
     * second extraction layer.
     *
     * @param callable $resolve
     * @param callable $reject
     * @return void
     */
    public function extractAttributes(callable $resolve, callable $reject): void;

    /**
     * Set appropriate taxonomies IDs (present in the database) for
     * the generated post into the taxonomies properties.
     *
     * @param callable $resolve
     * @param callable $reject
     *
     * @return void.
     */
    public function retrieveTaxonomies(callable $resolve, callable $reject): void;
}
