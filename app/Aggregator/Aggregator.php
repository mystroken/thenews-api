<?php
namespace App\Aggregator;

use App\Channel;
use App\Repositories\ChannelRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use React\Promise\PromiseInterface;
use function React\Promise\all;

/**
 * Class Aggregator
 *
 * Pulling data from RSS feeds and
 * populate the database.
 *
 * @package App\Aggregator
 * @author Mystro Ken <mystroken@gmail.com>
 */
class Aggregator {

    /**
     * Feeds the aggregator with some given channels.
     *
     * @param Model|Collection|null $channels
     *
     * @return PromiseInterface Whose value is an array of created posts.
     */
    public function feed($channels = null): PromiseInterface
    {
        // Get channels to pull.
        $channels = ($channels !== null) ? collect($channels) : Channel::all();

        $promises = $channels->map(static function ($channel) {
            return ChannelRepository::pull($channel);
        });

        // Returns a promise whose value is an array of generated posts.
        return all($promises->all())->then(static function ($postsArray) {
            return collect($postsArray)->flatMap(static function ($posts) {
                return $posts;
            })->all();
        });
    }
}
