<?php
namespace App\Aggregator\Support;

use App\Image;
use App\Post;
use App\Channel;
use App\Aggregator\Exceptions\TransformerException;
use App\Aggregator\Contracts\Transformer as TransformerContract;
use App\Repositories\ImageRepository;
use DateTime;
use React\Promise\Promise;
use SimplePie_Item;
use function React\Promise\all;

class Transformer implements TransformerContract
{

    /**
     * @var Post $post
     */
    protected $post;

    /**
     * The image repository.
     *
     * @var ImageRepository
     */
    protected $imageRepository;

    /**
     * @var  SimplePie_Item  $feedItem
     */
    protected $feedItem;

    /**
     * @var Channel $channel
     */
    protected $channel;

    /**
     * @var array The current post's taxonomies IDs.
     */
    protected $taxonomies = [];

    /**
     * @var array $attributes
     */
    protected $attributes = [
        'title'         => '',
        'type'          => 'news',
        'image'         => null,
        'description'   => '',
        'content'       => '',
        'slug'          => '',
        'link'          => '',
        'published'     => 1,
        'created_at'    => '',
    ];

    /**
     * Transformer constructor.
     *
     * @param Channel $channel
     */
    public function __construct(Channel $channel)
    {
        $this->channel = $channel;
        $this->imageRepository = new ImageRepository;
    }


    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * @param array $attributes
     * @return Transformer
     */
    public function setAttributes( array $attributes ): Transformer
    {
        $this->attributes = $this->mergeAttributes($this->getAttributes(), $attributes);
        return $this;
    }

    /**
     * Generates a filled news model.
     *
     * @return Post|null
     */
    public function generatePost(): Post
    {
        $this->post = new Post;

        // Fill the post.
        $attributes = $this->getAttributes();
        $image_url = $attributes['image'];
        $attributes['image'] = null;
        unset($attributes['image']);
        $this->post->fill($attributes);

        // Relationship linking (channel and image).
        $this->post->channel()->associate($this->channel);
        if (filter_var($image_url, FILTER_VALIDATE_URL) !== false) {
            // Create a new Image.
            $image = $this->imageRepository->storeFromURL($image_url);
            if ($image->id) {
                $this->post->image()->associate($image);
            }
        }

        return $this->post;
    }

    /**
     * Extract the necessary attributes from
     * a SimplePie item object and
     * return a fulfilled promise when
     * the job got done.
     *
     * @param SimplePie_Item $item
     * @return Promise Whose the fulfilled value is the generated post model.
     */
    public function extract( SimplePie_Item $item ): Promise
    {
        // Keeps the item for future use
        // in the extraction process.
        // And does some reset.
        $this->feedItem = $item;
        $this->taxonomies = [];

        // Returns a promise that
        // will be resolved when all the
        // extraction job will get done.
        return new Promise(function( callable $resolve, callable $reject ) {
            /**
             * Extracts the necessary
             * attributes from the feed item.
             */

            // Applies a first extraction layer:
            // The default extraction.

            $thumbnails = $this->feedItem->get_thumbnail();

            $image = is_array($thumbnails)
                ? serialize($thumbnails)
                : $thumbnails
            ;

            $this->setAttributes([
                'title'         => $this->feedItem->get_title(),
                'description'   => strip_tags(trim($this->feedItem->get_description())),
                'content'       => trim($this->feedItem->get_content()),
                'image'         => $image,
                'slug'          => $this->slugify($this->feedItem->get_title()),
                'link'          => $this->feedItem->get_permalink(),
                'created_at'    => new DateTime($this->feedItem->get_date()),
                'updated_at'    => new DateTime($this->feedItem->get_date()),
            ]);


            // Applies a second extraction layer:
            // Extraction from specific transformers (if necessary).
            $extractionProcess = all([

                // Extract attributes.
                //$this->callMethodWithPromise('extractAttributes'),
                new Promise(function ($resolve, $reject) {
                    $this->extractAttributes($resolve, $reject);
                }),

                // Extract taxonomies.
                //$this->callMethodWithPromise('retrieveTaxonomies'),
                new Promise(function ($resolve, $reject) {
                    $this->retrieveTaxonomies($resolve, $reject);
                })

            ]);

            // When the second extraction
            // layer has something for us.
            $extractionProcess ->then(function() use ($resolve) {

                // Here, the job has been done,
                // no errors have been generated and
                // the post model has been filled out as we wished.
                $post = $this->generatePost();
                $taxonomies = $this->getTaxonomies();

                // Resolves the promise and returns the post.
                $resolve([
                    'post'       => $post,
                    'taxonomies' => $taxonomies,
                ]);
            });

        });
    }


    /**
     * Second layer of the extraction process.
     *
     * When the job is done, we should
     * call the $resolve callback.
     *
     * If an error occurs, call $reject.
     *
     * @param callable $resolve
     * @param callable $reject
     */
    public function extractAttributes(callable $resolve, callable $reject): void
    {
        $resolve(null);
    }

    /**
     * @param callable $resolve
     * @param callable $reject
     */
    public function retrieveTaxonomies(callable $resolve, callable $reject): void
    {
        $resolve(null);
    }

    /**
     * Instantiate a new transformer
     * from the transformer key contained into the channel model
     *
     * @param Channel $channel
     *
     * @return TransformerContract
     * @throws TransformerException
     */
    public static function createFromChannel($channel): TransformerContract
    {
        $transformers = config('aggregator.transformers');
        $transformerKey = $channel->transformer;

        if (array_has($transformers, $transformerKey)) {

            if (array_has(class_implements($transformers[$transformerKey]), TransformerContract::class)) {
                return new $transformers[$transformerKey]($channel);
            }

            throw new TransformerException("Cannot instantiate $transformers[$transformerKey] Transformer!");
        }

        /**
         * Returns the default transformer
         * if any appropriate transformer can't be found.
         */
        return new Transformer($channel);
    }

    /**
     * Generates a slug for the news from a string.
     *
     * @param string $title
     * @return string
     */
    protected function slugify($title): string
    {
        return str_slug($title . '-' . uniqid('', false));
    }

    /**
     * Get the array of taxonomies IDs.
     *
     * @return array
     */
    public function getTaxonomies(): array
    {
        return $this->taxonomies;
    }

    /**
     * Set an array of some taxonomies IDs.
     *
     * @param array $taxonomies
     *
     * @return Transformer
     */
    public function setTaxonomies( array $taxonomies ): Transformer
    {
        $this->taxonomies = $taxonomies;
        return $this;
    }

    /**
     * Adds one taxonomy ID at a time.
     *
     * @param int $ID
     *
     * @return Transformer
     */
    public function addTaxonomy(int $ID): Transformer
    {
        $this->taxonomies[] = $ID;
        return $this;
    }

    /**
     * @param array $attributes
     * @param array $overwrites
     *
     * @return array
     */
    protected function mergeAttributes(array $attributes, array $overwrites): array
    {
        if (empty($overwrites)) {
            return $attributes;
        }

        $finalAttributes = [];

        foreach ($attributes as $key => $attributeValue)
        {
            $value = $overwrites[$key] ?? $attributeValue;
            $finalAttributes[$key] = $value;
        }

        return $finalAttributes;
    }

    /**
     * Allows the called method the
     * freedom to decide when it finished its
     * execution by passing the promise's callbacks to him.
     *
     * @param string $method
     *
     * @return Promise
     */
    protected function callMethodWithPromise(string $method): Promise
    {
        return new Promise(function ($resolve, $reject) use ($method) {
            call_user_func([$this,$method], [$resolve, $reject]);
        });
    }
}
