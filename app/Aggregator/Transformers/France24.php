<?php
namespace App\Aggregator\Transformers;

use App\Aggregator\Support\Transformer;

/**
 * France 24 Transformer
 *
 * @author Mystro Ken <mystroken@gmail.com>
 */
class France24 extends Transformer
{

    public function extractAttributes(callable $resolve, callable $reject): void
    {

        // Get the post image
        $thumbail = unserialize($this->getAttributes()['image']);

        if (is_array($thumbail)) {
            $this->setAttributes([
                'image' => $thumbail['url'],
            ]);
        }

        $resolve(null);
    }
}
