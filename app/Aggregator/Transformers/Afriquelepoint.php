<?php
/**
 * Created by PhpStorm.
 * User: CLAUDEL KROS
 * Date: 9/10/2018
 * Time: 2:39 PM
 */

namespace App\Aggregator\Transformers;


use App\Aggregator\Support\Transformer;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

class Afriquelepoint extends Transformer
{
    public function extractAttributes(callable $resolve, callable $reject): void
    {
        $item = $this->feedItem;
        $this->setAttributes([
            'image' => $item->get_enclosure()->link,
        ]);

        $client = new Client();
        $crawler = $client->request('GET', $this->getAttributes()['link']);


        $crawler->filter('article span.relative .mbs')->each(function (Crawler $node) use($resolve){
            $this->setAfriqueLePointTaxonomies($node->text());
        });

        $crawler->filter('article span.relative .mbs')->each(function (Crawler $crawler) {
            foreach ($crawler as $node) {
                $node->parentNode->removeChild($node);
            }
        });

        $crawler->filter('article span.relative')->each(function (Crawler $node) use($resolve){
            $this->setAttributes([
                'content' => $node->html(),
            ]);

        });

        $resolve(null);
    }

    public function setAfriqueLePointTaxonomies($taxonomies): void
    {
        // Transform categories array to Laravel Collection
        // This way we can take advantage of the FP methods.
        collect($taxonomies)
            // Each row contains a name of the taxonomy
            // (sometimes with spaces included) so
            // we need to trim each of them.
            ->map(function (string $categoryName) {
                return trim($categoryName);
            })
            // For each category in the array, find its
            // ID in database (if exists) and
            //send it to the transformer.
            ->each(function ($category) {
                \App\Taxonomy
                    ::where('name', 'like', $category)
                    ->get()
                    ->each(function ($taxonomy) {
                        $this->addTaxonomy($taxonomy->id);
                    });
            });
    }
}
