<?php
/**
 * Created by PhpStorm.
 * User: CLAUDEL KROS
 * Date: 9/2/2018
 * Time: 7:45 PM
 */

namespace App\Aggregator\Transformers;


use App\Aggregator\Support\Transformer;

class Africatopsports extends Transformer
{
    public function extractAttributes(callable $resolve, callable $reject): void
    {
        $item = $this->feedItem;

        $pattern_descrip = '#(<p>)(.+)\[.+]#';
        preg_match($pattern_descrip, trim($item->get_description()), $description);

        $pattern1 = '#https:\/\/[a-zA-Z.\/-]+[0-9\/]+[a-zA-Z-]+[7-9]+[0-9x.]+[a-zA-Z]+#';
        if (preg_match($pattern1, trim($item->get_content()), $matches)){
            $this->setAttributes([
                'image'     =>  $matches[0],
                'description' => $description[2],
            ]);
        }else{
            $pattern = '#srcset="((.)+),\s((.)+)"#U';
            preg_match($pattern, trim($item->get_content()), $matches);
            $this->setAttributes([
                'image'     =>  explode(' ', $matches[3])[0],
                'description' => $description[2],
            ]);
        }

        $resolve(null);
    }
}
