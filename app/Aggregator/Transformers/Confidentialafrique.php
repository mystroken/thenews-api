<?php
/**
 * Created by PhpStorm.
 * User: CLAUDEL KROS
 * Date: 8/31/2018
 * Time: 11:07 PM
 */

namespace App\Aggregator\Transformers;


use App\Aggregator\Support\Transformer;

class Confidentialafrique extends Transformer
{

    public function extractAttributes(callable $resolve, callable $reject): void
    {
        $item = $this->feedItem;

         $currentTitle = $this->getAttributes()['title'];
         if (strlen($currentTitle) > 255) {

             //$patterns = '#(.)*(\/)#';
             $pattern = '#(^(.)*)\s-\s#';

             if (preg_match($pattern, trim($item->get_title()), $matches)){

                 $this->setAttributes([
                     'title' => $matches[1],
                     'slug' => $this->slugify($matches[1]),
                 ]);
             }
         }

        $resolve(null);
    }
}
