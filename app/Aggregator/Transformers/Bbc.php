<?php

namespace App\Aggregator\Transformers;


use App\Aggregator\Support\Transformer;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

class Bbc extends Transformer
{
    public function extractAttributes(callable $resolve, callable $reject): void
    {
        $item = $this->feedItem;
        $image = $item->get_thumbnail()['url'];
        $this->setAttributes([
            'image' =>  $image,
        ]);

        $client = new Client();
        $crawler = $client->request('GET', $this->getAttributes()['link']);

        $crawler->filter('.story-body .story-body__h1')->each(function (Crawler $crawler) {
            foreach ($crawler as $node) {
                $node->parentNode->removeChild($node);
            }
        });

        $crawler->filter('.story-body .bbccom_slot')->each(function (Crawler $crawler) {
            foreach ($crawler as $node) {
                $node->parentNode->removeChild($node);
            }
        });

        $crawler->filter('.story-body .with-extracted-share-icons')->each(function (Crawler $crawler) {
            foreach ($crawler as $node) {
                $node->parentNode->removeChild($node);
            }
        });

        $crawler->filter('.story-body .share-tools--no-event-tag')->each(function (Crawler $crawler) {
            foreach ($crawler as $node) {
                $node->parentNode->removeChild($node);
            }
        });

        $crawler->filter('.story-body figure.media-landscape')->each(function (Crawler $crawler) {
            foreach ($crawler as $node) {
                $node->parentNode->removeChild($node);
            }
        });

        $crawler->filter('.story-body .story-body__introduction')->each(function (Crawler $crawler) {
            foreach ($crawler as $node) {
                $node->parentNode->removeChild($node);
            }
        });

        $crawler->filter('.story-body')->each(function (Crawler $node) use($resolve){
            $this->setAttributes([
                'content' => trim($node->html()),
            ]);
        });
        $resolve(null);
    }

}
