<?php
namespace App\Aggregator\Transformers;

use App\Models\News;
use App\Repositories\CategoryRepository;
use App\Aggregator\Support\Transformer;

class Ecofinance extends Transformer
{

    public function extractAttributes(callable $resolve, callable $reject): void
    {
        $item = $this->feedItem;

        $photos = $item->get_item_tags('http://www.pheed.com/pheed/', 'imgsrc');
        $photo = !is_null($photos) ? $photos[0]['data'] : null;

        $this->setAttributes([
            'image' => $photo,
        ]);

        $resolve(null);
    }
}
