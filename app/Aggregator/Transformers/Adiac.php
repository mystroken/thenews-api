<?php
/**
 * Created by PhpStorm.
 * User: CLAUDEL KROS
 * Date: 9/11/2018
 * Time: 2:42 PM
 */

namespace App\Aggregator\Transformers;


use App\Aggregator\Support\Transformer;
use App\Channel;
use Goutte\Client;
use React\Promise\Promise;
use Symfony\Component\DomCrawler\Crawler;

class Adiac extends Transformer
{
    public function extractAttributes(callable $resolve, callable $reject): void
    {

        $client = new Client();
        $crawler = $client->request('GET', $this->getAttributes()['link']);

        $crawler->filter('.field-items .field-item.even p img')->each(function (Crawler $node) use ($resolve) {

            $item = $this->feedItem;
            $image = $node->attr('src');
            $links = $item->get_link();
            $link = explode('/', $links);
            $this->setAttributes([
                'image' => $link[0].'//'.$link[2].$image,
            ]);

        });

        $crawler->filter('.node-content')->each(function (Crawler $node) use ($resolve) {

            $this->setAttributes([
                'content' => $node->html(),
            ]);

        });
        // Needs taxonomies to end this Implementation

        $resolve(null);
    }

}
