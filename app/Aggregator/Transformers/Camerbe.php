<?php
namespace App\Aggregator\Transformers;


use App\Aggregator\Support\Transformer;
use App\Taxonomy;
use Goutte\Client;
use SimplePie_Item;
use Symfony\Component\DomCrawler\Crawler;

class Camerbe extends Transformer
{
    /**
     * @param callable $resolve
     * @param callable $reject
     */
    public function extractAttributes(callable $resolve, callable $reject): void
    {

        /** @var SimplePie_Item $item */
        $item = $this->feedItem;

        /**
         * Retrieve news data
         */
        $matches = [];
        $pattern  = '#^<img src=["|\'](.*)["|\'] width=["|\'](.*)["|\'] alt=["|\'](.*)["|\'] title=["|\'](.*)["|\']>(.*)$#Usi';
        preg_match_all($pattern, trim($item->get_description()), $matches, PREG_SET_ORDER);
        $matches = (!empty($matches)) ? $matches[0] : [];

        /**
         * Fill attributes
         */
        $title          = (!empty($matches)) ? $matches[3] : $item->get_title();
        $description    = (!empty($matches)) ? $matches[5] : trim($item->get_description());
        $content        = (!empty($matches)) ? $matches[5] : $item->get_content();

        $this->setAttributes([
            'title'         => $title,
            'description'   => strip_tags($description),
            'content'       => $content,
            'slug'          => $this->slugify($title),
        ]);



        $client = new Client();
        $crawler = $client->request('GET', $this->getAttributes()['link']);

        $crawler->filter('article .card-img-top')->each(function (Crawler $node) {
            $image = $node->attr('data-src');

            $this->setAttributes([
                'image' => $image,
            ]);
        });


        $crawler->filter('article .badge')->each(function (Crawler $node) {
            $taxonomies = explode(' :: ', $node->text());
            $this->setCustomTaxonomies($taxonomies);
        });

        $resolve(null);
    }

    /**
     * @param array $taxonomies
     */
    public function setCustomTaxonomies(array $taxonomies): void
    {
        // Transform categories array to Laravel Collection
        // This way we can take advantage of the FP methods.
        collect($taxonomies)
            // Each row contains a name of the taxonomy
            // (sometimes with spaces included) so
            // we need to trim each of them.
            ->map(static function (string $categoryName) {
                return trim($categoryName);
            })
            // For each category in the array, find its
            // ID in database (if exists) and
            //send it to the transformer.
            ->each(function ($category) {
                Taxonomy
                    ::where('name', 'like', $category)
                    ->get()
                    ->each(function ($taxonomy) {
                        $this->addTaxonomy($taxonomy->id);
                    });
            });
    }


}
