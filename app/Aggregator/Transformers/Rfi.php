<?php

namespace App\Aggregator\Transformers;


use App\Aggregator\Support\Transformer;
use App\Taxonomy;
use Goutte\Client;
use React\Promise\Promise;
use Symfony\Component\DomCrawler\Crawler;

class Rfi extends Transformer
{
    /**
     * @param callable $resolve
     * @param callable $reject
     * @return void
     */
    public function extractAttributes(callable $resolve, callable $reject): void
    {
        $client = new Client();
        $crawler = $client->request('GET', $this->getAttributes()['link']);

        $finalExtractionPromise = \React\Promise\all([
            $this->retrieveImage($crawler),
            $this->retrieveContent($crawler),
        ]);

        $finalExtractionPromise->then(function() use ($resolve) {
            $resolve(null);
        });
    }

    /**
     * @param callable $resolve
     * @param callable $reject
     */
    public function retrieveTaxonomies( callable $resolve, callable $reject ): void
    {
        // Transform categories array to Laravel Collection
        // This way we can take advantage of the FP methods.
        collect(
            $this->feedItem->get_categories())
            // Each row contains a SimplePie_Category item
            // So we only return the category term
            ->map(function (\SimplePie_Category $category) {
                return $category->term;
            })
            // The RFI provides sometimes many categories in one
            // by separating them with slashes. So
            // we explode them into a new array
            ->map(function ($category) {
                return explode(' / ', $category);
            })
            // Here we get a two dimensional array. So
            // we flatten it to one dimension.
            ->flatMap(function ($value) {
                return $value;
            })
            // For each category in the array, find its
            // ID in database (if exists) and
            //send it to the transformer.
            ->each(function ($category) {
                Taxonomy
                    ::where('name', 'like', $category)
                    ->get()
                    ->each(function ($taxonomy) {
                        $this->addTaxonomy($taxonomy->id);
                    });
            });

        // Tell the transformer we're done.
        $resolve(null);
    }

    /**
     * Retrieves correct size of image.
     *
     * @param Crawler $crawler
     * @return Promise
     */
    protected function retrieveImage(Crawler $crawler)
    {
        return new Promise(function ($resolve) use ($crawler) {
            $crawler->filter('#article-details header.wrapper .block-image noscript img')->each(function (Crawler $node) use ($resolve) {

                $image = $node->attr('src');

                $this->setAttributes([
                    'image' => $image,
                ]);

                $resolve(null);
            });
        });
    }

    /**
     * Retrieves the post content.
     *
     * @param Crawler $crawler
     * @return Promise
     */
    protected function retrieveContent(Crawler $crawler)
    {
        return new Promise(function ($resolve) use ($crawler) {

            // Remove pub block.
            $crawler->filter('#article-details article .intro .pub-content')->each(function (Crawler $crawler) {
                foreach ($crawler as $node) {
                    $node->parentNode->removeChild($node);
                }
            });

            // Remove embed content.
            $crawler->filter('#article-details article .intro .em-block')->each(function (Crawler $crawler) {
                foreach ($crawler as $node) {
                    $node->parentNode->removeChild($node);
                }
            });

            // Remove the description from the content.
            $crawler->filter('#article-details article .intro h2')->first()->each(function (Crawler $crawler) {
                foreach ($crawler as $node) {
                    $node->parentNode->removeChild($node);
                }
            });

            $crawler->filter('#article-details article .intro')->each(function (Crawler $node) use ($resolve) {

                $content = trim(preg_replace("/[^\\S\r\n]+/", " ", $node->html()));

                $this->setAttributes([
                    'content' => $content,
                ]);

                $resolve(null);
            });


        });
    }


}
