<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Transformers
    |--------------------------------------------------------------------------
    |
    | The transformers listed here will be automatically loaded to transform
    | RSS data to News.
    |
    */
    'transformers' => [
        'camerbe'               => App\Aggregator\Transformers\Camerbe::class,
        'ecofinance'            => App\Aggregator\Transformers\Ecofinance::class,
        'france24'              => App\Aggregator\Transformers\France24::class,
        'confidentialafrique'   => App\Aggregator\Transformers\Confidentialafrique::class,
        'latribune'             => App\Aggregator\Transformers\Latribune::class,
        'rfi'                   => \App\Aggregator\Transformers\Rfi::class,
        'africatopsports'       => App\Aggregator\Transformers\Africatopsports::class,
        'afriquelepoint'        => App\Aggregator\Transformers\Afriquelepoint::class,
        'adiac'                 => App\Aggregator\Transformers\Adiac::class,
        'bbc'                 => App\Aggregator\Transformers\Bbc::class,
    ],

];
