# The news API

> A news aggregator and community platform for africans. Using: [Laravel 5.6](//laravel.com/docs/5.6/) — [JWT Auth](//github.com/tymondesigns/jwt-auth/) — [Dingo Api](//github.com/dingo/api)

**Table of contents**

[Introduction](#introduction)

## Introduction

The project is developed on top of Laravel 5.6 using our custom api development boilerplate.

Have a look on it to really get started. [Laravel API Boilerplate —]([https://github.com/mystroken/laravel-api-boilerplate](https://github.com/mystroken/laravel-api-boilerplate)

#### — Installation

Clone the project locally:

```bash
git clone https://bitbucket.org/mystroken/thenews-api.git
```

Install project dependencies:

```bash
cd thenews-api && composer install
```

Set your environment parameters:

```
cp .env.exemple .env
```

Finally, generate security keys:

```
php artisan key:generate
```

```
php artisan jwt:secret
```

#### — Good to know

Inside the project you'll find these patterns:

- Repository pattern

#### — Add a new endpoint

* Write Tests

* Add route

* Create Model

* Create Resource

* Create Repository

* Create Controller

* Create Request (Validation)
