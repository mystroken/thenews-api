<?php

use Illuminate\Http\Request;
use Dingo\Api\Routing\Router;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/** @var Router $api */
$api = app(Router::class);
$api->group([
    'version' => 'v1',
    'namespace' => 'App\Http\Controllers\Api',
], function (Router $api){

    /*
    |----------------------------------------------------------------------
    | Protected Routes
    |----------------------------------------------------------------------
    |
    */
    $api->group(['middleware' => 'api.auth'], function(Router $api){


        /**
         * AUTH PROTECTED ENDPOINTS
         * -----------------------------------------------------------
         */
        $api
            ->get('token','AuthenticateController@getToken')
            ->name('api.token')
        ;
        $api
            ->post('logout','AuthenticateController@logout')
            ->name('api.logout')
        ;
        $api
            ->get('authenticated_user', 'AuthenticateController@authenticatedUser')
            ->name('api.authenticated_user')
        ;


        /**
         * USERS PROTECTED ENDPOINTS
         * -----------------------------------------------------------
         */
        $api
            ->put( 'users/{user}', 'UserController@update' )
            ->where( 'user', '[0-9]+' )
            ->name( 'api.users.update' )
        ;
        $api
            ->delete( 'users/{user}', 'UserController@destroy' )
            ->where( 'user', '[0-9]+' )
            ->name( 'api.users.destroy' )
        ;


        /**
         * POSTS PROTECTED ENDPOINTS
         * -----------------------------------------------------------
         */
        $api
            ->post( 'posts', 'PostController@store' )
            ->name( 'api.posts.store' )
        ;
        $api
            ->put( 'posts/{postId}', 'PostController@update' )
            ->where( 'postId', '^[a-zA-Z0-9_-]+$' )
            ->name( 'api.posts.update' )
        ;
        $api
            ->delete( 'posts/{postId}', 'PostController@destroy' )
            ->where( 'postId', '^[a-zA-Z0-9_-]+$' )
            ->name( 'api.posts.destroy' )
        ;
    });

    /*
    |----------------------------------------------------------------------
    | Non-Protected Routes
    |----------------------------------------------------------------------
    |
    */
    /**
     * AUTH NON-PROTECTED ENDPOINTS
     * -----------------------------------------------------------
     */
    $api
        ->post('authenticate', 'AuthenticateController@authenticate')
        ->name('api.authenticate')
    ;

    /**
     * USERS NON-PROTECTED ENDPOINTS
     * -----------------------------------------------------------
     */
    $api
        ->get( 'users', 'UserController@index' )
        ->name( 'api.users.list' )
    ;
    $api
        ->get( 'users/{user}/posts', 'UserController@posts' )
        ->where( 'user', '^[a-zA-Z0-9_-]+$' )
        ->name( 'api.users.posts' )
    ;
    $api
        ->get( 'users/{user}', 'UserController@show' )
        ->where( 'user', '^[a-zA-Z0-9_-]+$' )
        ->name( 'api.users.show' )
    ;
    $api
        ->post( 'users', 'UserController@store' )
        ->name( 'api.users.store' )
    ;

    /**
     * TAXONOMIES NON-PROTECTED ENDPOINTS
     * -----------------------------------------------------------
     */
    $api
        ->get( 'taxonomies', 'TaxonomyController@index' )
        ->name( 'api.taxonomies.list' )
    ;
    $api
        ->get( 'taxonomies/{taxonomiesId}/posts', 'TaxonomyController@posts' )
        ->where( 'taxonomiesId', '^[a-zA-Z0-9_-]+$' )
        ->name( 'api.taxonomies.posts' )
    ;
    $api
        ->get( 'taxonomies/{taxonomiesId}', 'TaxonomyController@show' )
        ->where( 'taxonomiesId', '^[a-zA-Z0-9_-]+$' )
        ->name( 'api.taxonomies.show' )
    ;

    /**
     * CHANNELS NON-PROTECTED ENDPOINTS
     * -----------------------------------------------------------
     */
    $api
        ->get( 'channels', 'ChannelController@index' )
        ->name( 'api.channels.list' )
    ;
    $api
        ->get( 'channels/{channelId}', 'ChannelController@show' )
        ->where( 'channelId', '^[a-zA-Z0-9_-]+$' )
        ->name( 'api.channel.show' )
    ;

    /**
     * POSTS NON-PROTECTED ENDPOINTS
     * -----------------------------------------------------------
     */
    $api
        ->get( 'posts', 'PostController@index' )
        ->name( 'api.posts.list' )
    ;
    $api
        ->get( 'posts/{postId}', 'PostController@show' )
        ->where( 'postId', '^[a-zA-Z0-9_-]+$' )
        ->name( 'api.posts.show' )
    ;
});
