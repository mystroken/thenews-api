<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');

            // Foreign Keys.
            $table->unsignedInteger('channel_id')->nullable()->comment('the post\'s channel id');
            $table->unsignedInteger('user_id')->nullable()->comment('the post\'s author id');
            $table->unsignedInteger('image_id')->nullable()->comment('the post\'s image id');

            // Others table rows.
            $table->string('title')->nullable()->comment('the post title');
            $table->string('slug')->nullable()->comment('the post slug');
            $table->string('type', 100)->comment('the post type');
            $table->text('description')->nullable()->comment('the post description');
            $table->longText('content')->nullable()->comment('the post content');
            $table->string('link')->nullable()->comment('the external post link');
            $table->boolean('published')->nullable()->default(0)->comment('1:the post has been pusblished; 0:otherwise');
            $table->boolean('deleted')->nullable()->default(0)->comment('1:the post has been deleted; 0:otherwise');
            $table->timestamps();

            // Some table indexes.
            $table->unique([
                'slug',
                'published',
            ]);

        });

        // Foreign keys references.
        Schema::table('posts', function (Blueprint $table) {
            $table->foreign('channel_id')->references('id')->on('channels')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('image_id')->references('id')->on('images')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop Foreign keys references.
        Schema::table('posts', function (Blueprint $table) {
            $table->dropForeign([
                'channel_id',
                'user_id',
                'image_id'
            ]);
        });

        Schema::dropIfExists('posts');
    }
}
