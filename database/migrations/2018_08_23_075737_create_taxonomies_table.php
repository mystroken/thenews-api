<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxonomiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taxonomies', function (Blueprint $table) {
            $table->increments('id');

            // Foreign Keys.
            $table->unsignedInteger('parent_id')->nullable()->comment('the taxonomy\'s parent id');

            // Others table rows.
            $table->string('name')->comment('The taxonomy\'s name');
            $table->string('slug')->comment('The taxonomy\'s slug');
            $table->string('type', 100)->comment('The taxonomy\'s type');

            // Some table indexes.
            $table->unique([
                'type',
                'slug',
            ]);
        });

        // Foreign keys references.
        Schema::table('taxonomies', function (Blueprint $table) {
            $table->foreign('parent_id')->references('id')->on('taxonomies')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop Foreign keys references.
        Schema::table('taxonomies', function (Blueprint $table) {
            $table->dropForeign([
                'parent_id',
            ]);
        });

        Schema::dropIfExists('taxonomies');
    }
}
