<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('channels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('Channel name');
            $table->string('slug')->comment('Channel slug');
            $table->string('logo')->default('channel.jpg');
            $table->text('description')->nullable()->comment('Channel description');
            $table->string('url')->comment('Channel url');
            $table->text('feed')->nullable()->comment('Channel feed url');
            $table->string('transformer')->nullable()->comment('Channel transformer (data retriever)');
            $table->timestamp('fetched_at')->nullable()->comment('Channel last fetching date');
            $table->timestamps();

            $table->unique('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('channels');
    }
}
