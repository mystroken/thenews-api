<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostTaxonomiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_taxonomies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('post_id')->comment('The post id');
            $table->unsignedInteger('taxonomy_id')->comment('The taxonomy id');
        });

        // Foreign keys references.
        Schema::table('post_taxonomies', function (Blueprint $table) {
            $table->foreign('post_id')->references('id')->on('posts')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('taxonomy_id')->references('id')->on('taxonomies')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop Foreign keys references.
        Schema::table('post_taxonomies', function (Blueprint $table) {
            $table->dropForeign([
                'post_id',
                'taxonomy_id',
            ]);
        });

        Schema::dropIfExists('post_taxonomies');
    }
}
