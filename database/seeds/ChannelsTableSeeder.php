<?php

use Illuminate\Database\Seeder;

class ChannelsTableSeeder extends Seeder
{
    static $MAX = 5;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $camerbe = new App\Channel();
        $camerbe->name = 'Camer.be';
        $camerbe->slug = str_slug($camerbe->name);
        $camerbe->url  = 'https://www.camer.be';
        $camerbe->feed = 'https://www.camer.be/rss';
        $camerbe->transformer = 'camerbe';
//        $camerbe->save();

        // This channel website is down
        // for the moment.
        $ecofinance = new App\Channel();
        $ecofinance->name = 'Ecofinance';
        $ecofinance->slug = str_slug($ecofinance->name);
        $ecofinance->url  = 'http://www.ecofinance.sn';
        $ecofinance->feed = 'http://www.ecofinance.sn/xml/syndication.rss';
        $ecofinance->transformer = 'ecofinance';
//        $ecofinance->save();

        $france24 = new App\Channel();
        $france24->name = 'Afrique – France 24';
        $france24->slug = str_slug($france24->name);
        $france24->url  = 'http://www.france24.com/fr/afrique';
        $france24->feed = 'http://www.france24.com/fr/afrique/rss';
        $france24->transformer = 'france24';
//        $france24->save();

        $confidentialafrique = new App\Channel();
        $confidentialafrique->name = 'Confidentiel Afrique';
        $confidentialafrique->slug = str_slug($confidentialafrique->name);
        $confidentialafrique->url  = 'http://confidentielafrique.com';
        $confidentialafrique->feed = 'http://confidentielafrique.com/feed/';
        $confidentialafrique->transformer = 'confidentialafrique';
//        $confidentialafrique->save();

        $latribune = new App\Channel();
        $latribune->name = 'La Tribune Afrique';
        $latribune->slug = str_slug($latribune->name);
        $latribune->url  = 'https://afrique.latribune.fr/';
        $latribune->feed = 'https://afrique.latribune.fr/rss/rubriques/la-tribune-afrique-rss';
        $latribune->transformer = 'latribune';
//        $latribune->save();

        $rfiafrique = new App\Channel();
        $rfiafrique->name = 'Rfi Afrique';
        $rfiafrique->slug = str_slug($rfiafrique->name);
        $rfiafrique->url  = 'http://www.rfi.fr/afrique';
        $rfiafrique->feed = 'http://www.rfi.fr/afrique/rss/';
        $rfiafrique->transformer = 'rfi';
//        $rfiafrique->save();

        $afriquelepoint = new App\Channel();
        $afriquelepoint->name = 'Le point Afrique';
        $afriquelepoint->slug = str_slug($afriquelepoint->name);
        $afriquelepoint->url  = 'http://afrique.lepoint.fr';
        $afriquelepoint->feed = 'http://afrique.lepoint.fr/rss.xml';
        $afriquelepoint->transformer = 'afriquelepoint';
//        $afriquelepoint->save();

        $africatopsports = new App\Channel();
        $africatopsports->name = 'Africa tops Sport';
        $africatopsports->slug = str_slug($africatopsports->name);
        $africatopsports->url  = 'https://www.africatopsports.com/';
        $africatopsports->feed = 'https://www.africatopsports.com/feed/';
        $africatopsports->transformer = 'africatopsports';
        $africatopsports->save();
//
        $adiac = new App\Channel();
        $adiac->name = 'Toute l’information du Bassin du Congo';
        $adiac->slug =  str_slug($adiac->name);
        $adiac->url  =  'http://www.adiac-congo.com';
        $adiac->feed =  'http://www.adiac-congo.com/flux_rss_home';
        $adiac->transformer = 'adiac';
        $adiac->save();

        $bbc = new App\Channel();
        $bbc->name = 'BBC';
        $bbc->slug =  str_slug($bbc->name);
        $bbc->url  =  'http://www.bbc.com';
        $bbc->feed =  'http://feeds.bbci.co.uk/afrique/region/rss.xml';
        $bbc->transformer = 'bbc';
//        $bbc->save();
    }
}
