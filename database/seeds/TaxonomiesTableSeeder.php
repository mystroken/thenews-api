<?php

use Illuminate\Database\Seeder;

class TaxonomiesTableSeeder extends Seeder
{
    static $MAX = 14;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createCountryTaxonomies();

        $this->createCategoryTaxonomy('Société');
        $this->createCategoryTaxonomy('Finances');
        $this->createCategoryTaxonomy('Politique');
        $this->createCategoryTaxonomy('Sport');
        $this->createCategoryTaxonomy('Tech');
        $this->createCategoryTaxonomy('Startups');
        $this->createCategoryTaxonomy('Les Experts');
        $this->createCategoryTaxonomy('L\'annuaire');
        $this->createCategoryTaxonomy('Womakers');

    }

    /**
     * Create a new category.
     *
     * @param  string  $name  The category name.
     *
     * @return \App\Taxonomy
     */
    protected function createCategoryTaxonomy($name)
    {
        $t = new \App\Taxonomy();
        $t->name = $name;
        $t->slug = str_slug($name);
        $t->type = 'category';

        $t->save();
        return $t;
    }

    protected function createCountryTaxonomies()
    {
        $countries = [
            'Afrique du Sud',
            'Algérie',
            'Angola',
            'Bénin',
            'Botswana',
            'Burkina Faso',
            'Burundi',
            'Cameroun',
            'Centrafrique',
            'Comores',
            'Côte d\'Ivoire',
            'Djibouti',
            'Égypte',
            'Érythrée',
            'Éthiopie',
            'Gabon',
            'Gambie',
            'Ghana',
            'Guinée',
            'Guinée équatoriale',
            'Guinée-Bissau',
            'Kenya',
            'Libéria',
            'Libye',
            'Madagascar',
            'Malawi',
            'Mali',
            'Maroc',
            'Mauritanie',
            'Mozambique',
            'Niger',
            'Nigeria',
            'Ouganda',
            'RDC',
            'Rwanda',
            'Sao Tomé-et-Principe',
            'Sénégal',
            'Seychelles',
            'Sierra Leone',
            'Somalie',
            'Soudan',
            'Swaziland',
            'Tanzanie',
            'Tchad',
            'Togo',
            'Tunisie',
            'Zambie',
            'Zimbabwe',
        ];

        foreach ($countries as $country) {
            $t = new \App\Taxonomy();
            $t->name = $country;
            $t->slug = str_slug($country);
            $t->type = 'country';

            $t->save();
        }
    }
}
