<?php

use App\Aggregator\Aggregator;
use App\Post;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    public static $MAX = 5;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        // Create posts
        factory(Post::class, static::$MAX)->create()->each(function (Post $p) {
            $p->author()->associate($this->generateAuthorId(UsersTableSeeder::$MAX))->save();
            if ($p->id%2 !== 0) {
                $p->taxonomies()->attach($this->generateTaxonomiesIds(random_int(1, 4)));
            }
        });

        // Fetch News
        $aggregator = new Aggregator();
        $aggregator->feed();
    }

    /**
     * Generate an array of ID.
     *
     * @param int $nb
     *
     * @return int
     * @throws Exception
     */
    protected function generateAuthorId(int $nb): int
    {
        return random_int(1, $nb);
    }

    /**
     * @param int $nb
     *
     * @return array
     * @throws Exception
     */
    protected function generateTaxonomiesIds(int $nb): array
    {
        $IDs = [];
        for($i=0; $i<$nb; $i++) {
            $IDs[] = random_int($i + 1, TaxonomiesTableSeeder::$MAX);
        }
        return $IDs;
    }
}
