<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    static $MAX = 3;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, static::$MAX)->create();

        // Users for testing
        $user = new App\User;
        $user->fill([
            'name' => 'Mystro Ken',
            'username' => 'mystroken',
            'email' => 'mystroken@gmail.com',
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
            'remember_token' => str_random(10),
        ]);
        $user->save();
    }
}
