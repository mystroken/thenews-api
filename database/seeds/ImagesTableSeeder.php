<?php

use Illuminate\Database\Seeder;

class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $image = new \App\Image();
        $image->original = 'https://picsum.photos/1920/1080';
        $image->medium = '';
        $image->small = '';
        $image->save();
    }
}
