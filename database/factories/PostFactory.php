<?php

use Faker\Generator as Faker;

$factory->define(\App\Post::class, function (Faker $faker) {
    $title = $faker->realText(60);

    return [
        'title' => $title,
        'slug' => str_slug($title),
        'type' => 'post',
        'description' => $faker->paragraph(),
        'content' => $faker->paragraphs(4, true),
        'image_id' => 1,
        'published' => 1,
    ];
});
